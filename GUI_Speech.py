import wave
import threading
import tkinter
import tkinter.filedialog
import tkinter.messagebox
import pyaudio
from SpeechRecongize import getText
# binStart = tkinter.Button(root, text='Start', command=start)
# binStart.place(x=30, y=20, width=100, height=20)
#
# binStop = tkinter.Button(root, text='Stop', command=stop)
# binStop.place(x=140, y=20, width=100, height=20)


from TestRe.SpeechRecongize import getText

from musicList import MusicList
from musicButtonControl import MusicButtonControl
from musicLyricText import MusicLyricText

root = tkinter.Tk()
root.title("语音识别客户端")
root.geometry("700x500+200+100")
root.resizable(False, False)

# def record():
#     global fileName
#     p = pyaudio.PyAudio()
#     # audio流对象
#     stream = p.open(format=FORMAT,
#                     channels=CHANNELS,
#                     rate=RATE,
#                     input=True,
#                     frames_per_buffer=CHUNK_SIZE)
#     # 音频文件对象
#     wf = wave.open(fileName, 'wb')
#     wf.setnchannels(CHANNELS)
#     wf.setsampwidth(p.get_sample_size(FORMAT))
#     wf.setframerate(RATE)
#
#     # 读取数据写入文件
#     while allowRecording:
#         data = stream.read(CHUNK_SIZE)
#         wf.writeframes(data)
#     wf.close()
#     stream.stop_stream()
#     stream.close()
#     p.terminate()
#     fileName = None
#
#
# def start():
#     global allowRecording, fileName
#     fileName = tkinter.filedialog.asksaveasfilename(filetypes=[('未压缩文件', '*.wav')])
#     if not fileName:
#         return
#     if not fileName.endswith('.wav'):
#         fileName = fileName + '.wav'
#     allowRecording = True
#     lbStatus['text'] = '状态：录音Recoding...'
#     threading.Thread(target=record).start()
#
#
# def stop():
#     global allowRecording, fileName
#     allowRecording = False
#     lbStatus['text'] = '状态：准备录音Ready'
#     lbSpeech_Recognition['text'] = "正在识别,请稍等...."
#     # 开始识别
#     text = "未识别"
#     # 载入wav文件 和 模型
#     hanzitext = getText(fileName)
#     print("识别结果：", hanzitext)
#     text = hanzitext
#     lbSpeech_Recognition['text'] = "识别结果：" + text


# 关闭程序时检查是否正在录制

# def closeWindow():
#     if allowRecording:
#         tkinter.messagebox.showerror('Recording', 'Please stop recording before close the window.')
#         return
#     root.destroy()


def getMusicDist(keyword):
    import time, os, requests
    musicDist = {}
    os.system('')
    # keyword = input('\033[33m来源：酷狗音乐(少数付费歌曲只能下载试听的一分钟）\n搜索歌曲:\033[0m')
    time = time.time() * 1000
    url = f'https://songsearch.kugou.com/song_search_v2?callback=jQuery112405132987859127838_{time}' \
          f'&page=1&pagesize=30&userid=-1&clientver=&platform=WebFilter&tag=em&filter=2&iscorrection=1&privilege_filter=0&_={time}&keyword={keyword}'
    headers = {
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36'}
    resp1 = requests.get(url, headers=headers).text
    s1 = resp1.find('{')
    resp1 = resp1[s1:-2]
    jd = json.loads(resp1)  # 字符串转字典
    for i in jd['data']['lists']:
        a = i['FileHash']
        b = i['AlbumID']
        url1 = f'https://wwwapi.kugou.com/yy/index.php?r=play/getdata&callback' \
               f'=jQuery19108991040761831859_1607742705511&hash=' \
               f'{a}&dfid=334lgQ3gvzHD1503TJ1eRVym&mid=449fc6a9349f6b64fc0d58efab406b8d&platid=4&album_id' \
               f'={b}&_=1607742705512'
        resp2 = requests.get(url=url1, headers=headers).text
        s2 = resp2.find('{')
        resp2 = resp2[s2:-2]
        jd1 = json.loads(resp2)  # 字符串转字典
        print('歌名\033[36m', jd1['data']['audio_name'])
        print('\033[0m专辑名', jd1['data']['album_name'])
        print('下载链接\033[33m', jd1['data']['play_url'], '\033[0m')
        musicDist[jd1['data']['audio_name']] = jd1['data']['play_url']
    # input('\033[32m按enter退出\033[0m')
    return musicDist


# binStart = tkinter.Button(root, text='Start', command=start)
# binStart.place(x=30, y=20, width=100, height=20)
#
# binStop = tkinter.Button(root, text='Stop', command=stop)
# binStop.place(x=140, y=20, width=100, height=20)
#

lbStatus = tkinter.Label(root, text='状态：准备录音Ready', anchor='w', fg='green')  # 靠左显示绿色状态字
lbStatus.place(x=30, y=50, width=200, height=20)

lbSpeech_Recognition = tkinter.Label(root, text='识别结果：略', anchor='w', fg='blue')
lbSpeech_Recognition.place(x=30, y=70, width=300, height=20)


# root.protocol('WM_DELETE_WINDOW', closeWindow)


if __name__ == '__main__':
    # 音乐播放器
    musicList = MusicList(root)
    musicButton = MusicButtonControl(root, musicList)
    musicLyric = MusicLyricText(root)
    root.mainloop()
