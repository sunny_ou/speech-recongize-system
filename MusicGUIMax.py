# coding = utf-8
from tkinter import *
import tkinter.messagebox
import requests
from lxml import etree
from urllib.request import urlretrieve
from tkinter.filedialog import askdirectory
import json
import requests
import time
import json
import os


head = {
    'Host': 'songsearch.kugou.com',
    'Origin': 'http://www.kugou.com/',
    'Referer': 'http://www.kugou.com/',
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36'
}

header2 = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36'
}

mp3_info = {}  # 全局变量，存放歌曲名和hash


def getMusicDist(keyword):
    import time
    musicDist = {}
    os.system('')
    # keyword = input('\033[33m来源：酷狗音乐(少数付费歌曲只能下载试听的一分钟）\n搜索歌曲:\033[0m')
    time = time.time() * 1000
    url = f'https://songsearch.kugou.com/song_search_v2?callback=jQuery112405132987859127838_{time}' \
          f'&page=1&pagesize=30&userid=-1&clientver=&platform=WebFilter&tag=em&filter=2&iscorrection=1&privilege_filter=0&_={time}&keyword={keyword}'
    headers = {
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36'}
    resp1 = requests.get(url, headers=headers).text
    s1 = resp1.find('{')
    resp1 = resp1[s1:-2]
    jd = json.loads(resp1)  # 字符串转字典
    for i in jd['data']['lists']:
        a = i['FileHash']
        b = i['AlbumID']
        url1 = f'https://wwwapi.kugou.com/yy/index.php?r=play/getdata&callback' \
               f'=jQuery19108991040761831859_1607742705511&hash=' \
               f'{a}&dfid=334lgQ3gvzHD1503TJ1eRVym&mid=449fc6a9349f6b64fc0d58efab406b8d&platid=4&album_id' \
               f'={b}&_=1607742705512'
        resp2 = requests.get(url=url1, headers=headers).text
        s2 = resp2.find('{')
        resp2 = resp2[s2:-2]
        jd1 = json.loads(resp2)  # 字符串转字典
        print('歌名\033[36m', jd1['data']['audio_name'])
        print('\033[0m专辑名', jd1['data']['album_name'])
        print('下载链接\033[33m', jd1['data']['play_url'], '\033[0m')
        musicDist[jd1['data']['audio_name']] = jd1['data']['play_url']
    # input('\033[32m按enter退出\033[0m')
    return musicDist


# 选择文件路径的函数
def selectPath():
    path_ = askdirectory()
    path.set(path_)


def help_info():
    tkinter.messagebox._show('v0.0.1帮助',
                             '输入下载的歌曲名.单曲搜索结果选中某行后再进行下载,重新搜索记得清空列表\n暂不支持:\n歌手搜索(歌手搜索会有结果,但不能下载,因为是酷狗音源,懒得去写)\n歌单搜索(其实是网易云搜索字段还没有看懂原理)\n新手第一个小程序,如有bug,那就不管了')


def cleartxt():
    global end
    text.delete(0, end)


def show():
    global end
    text.delete(0, end)
    song = entry.get()  # 获得歌曲名
    musicDist = getMusicDist(song)
    musicName = musicDist.keys()
    data = musicName  # 这是一个列表
    num = 0
    for ite in data:
        # 存放url
        mp3_info[ite] = musicDist[ite]
        text.insert(num, ite)
        num += 1
    end = num


def download():
    if not entry_path.get():
        tkinter.messagebox._show('错误', '没有选中文件夹路径')
        return
    pre_path = entry_path.get()
    content = entry.get()
    if content:
        num = text.curselection()[0]  # 结果是一个一维元组如(5,)
        if num != None:  # 选择的是num首歌，对应的data[num] ，listbox下标从0开始
            mp3_name = text.get(num)
            mp3_url = mp3_info.get(mp3_name)  # url
            print("下载：", mp3_url)
            musicContent = requests.get(mp3_url).content
            try:
                path = pre_path + '/' + mp3_name + '.mp3'
                print("路径：", path)
                if '\\' in path:
                    path = path.replace('\\', '/')
                # 下载
                with open(path, 'ab') as file:
                    file.write(musicContent)
                    file.flush()
                print("下载成功==>", mp3_name)
                tkinter.messagebox._show('提示', '下载成功')
                return
            except Exception as e:
                print("写入文件失败,原因：", e)
                return


end = 0
root = Tk()
path = StringVar()
root.title("音乐下载器v0.0.1")
root.geometry("550x330+550+230")

Label(root, text="酷狗单曲名", font=('Consolas', 12)).grid(row=0, column=0)
Button(root, text="搜索", relief='ridge', font=("Consolas", 12), command=show).grid(row=0, column=2)

entry = Entry(root, font=('Consolas', 12))
entry.grid(row=0, column=1)

Label(root, text="文件存放路径", font=('Consolas', 12)).grid(row=2, column=0)
# 存放路径的输入栏
entry_path = Entry(root, textvariable=path, font=('Consolas', 12))
entry_path.grid(row=2, column=1)

Button(root, text="路径", relief='ridge', font=("Consolas", 12), command=selectPath).grid(row=2, column=2)  # ,sticky=E)

text = Listbox(root, selectmode=BROWSE, font=("Consolas", 12), width=45, height=10)
text.grid(row=3, columnspan=2)
Button(root, text="清空列表", relief='ridge', font=("Consolas", 12), command=cleartxt).grid(row=3, column=3, sticky=S)

# 下载和退出按钮
btn_down = Button(root, text="开始下载", relief='ridge', font=("Consolas", 12), command=download).grid(row=4, column=0,
                                                                                                   sticky=W)
Button(root, text="退出", relief='ridge', font=("Consolas", 12), command=root.quit).grid(row=4, column=1, sticky=E)
Button(root, text="帮助", relief='ridge', font=("Consolas", 12), command=help_info).grid(row=4, column=3, sticky=E)


root.mainloop()
