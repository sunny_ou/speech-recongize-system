import scipy.io.wavfile as wav
import matplotlib.pyplot as plt
import os
import numpy as np
from scipy.fftpack import fft
import matplotlib.pyplot as plt
import tensorflow as tf
import keras
from keras.layers import Input, Conv2D, BatchNormalization, MaxPooling2D
from keras.layers import Reshape, Dense, Lambda, Dropout
from keras.optimizers import Adam
from keras import backend as K
from keras.models import Model
from tqdm import tqdm
from scipy.fftpack import dct
from keras.utils import multi_gpu_model

from fun.calEnergy_calZeroCrossingRate import endPointReTurnNp


# 预加重
def H(file, u):
    return np.append(file[0], file[1:] - u * file[:-1])


def compute_fbank(file):
    # x = np.linspace(0, 400 - 1, 400, dtype=np.int64)
    # w = 0.54 - 0.46 * np.cos(2 * np.pi * (x) / (400 - 1))  # 汉明窗
    # fs, wavsignal = wav.read(file)
    # # 1、预加重
    # u = 0.9375
    # wavsignal = H(wavsignal, u)
    # # 端点检测
    # # 端点检验
    # wavsignal = endPointReTurnNp(wavsignal)
    # # wav波形 加时间窗以及时移10ms
    # time_window = 25  # 单位ms
    # window_length = fs / 1000 * time_window  # 计算窗长度的公式，目前全部为400固定值
    # wav_arr = np.array(wavsignal)
    # wav_length = len(wavsignal)
    # range0_end = int(len(wavsignal) / fs * 1000 - time_window) // 10  # 计算循环终止的位置，也就是最终生成的窗数
    # data_input = np.zeros((range0_end, 200), dtype=np.float)  # 用于存放最终的频率特征数据
    # data_line = np.zeros((1, 400), dtype=np.float)
    # for i in range(0, range0_end):
    #     p_start = i * 160
    #     p_end = p_start + 400
    #     data_line = wav_arr[p_start:p_end]
    #     data_line = data_line * w  # 加窗
    #     data_line = np.abs(fft(data_line))
    #     data_input[i] = data_line[0:200]  # 设置为400除以2的值（即200）是取一半数据，因为是对称的
    # data_input = np.log(data_input + 1)
    # # data_input = data_input[::]
    # return data_input
    # x = np.linspace(0, 400 - 1, 400, dtype=np.int64)
    # w = 0.54 - 0.46 * np.cos(2 * np.pi * (x) / (400 - 1))  # 汉明窗
    fs, wavsignal = wav.read(file)
    # 1、预加重
    u = 0.9375
    wavsignal = H(wavsignal, u)
    # 端点检测
    # 端点检验单独进行
    plt.plot(wavsignal)
    plt.show()
    wavsignal = endPointReTurnNp(wavsignal)
    plt.plot(wavsignal)
    plt.show()
    # 2、分帧
    # wav波形 加时间窗以及时移10ms
    # time_window = 25  # 单位ms
    # wav_arr = np.array(wavsignal)
    frame_size = 0.025
    frame_stride = 0.01
    frame_length, frame_step = frame_size * fs, frame_stride * fs  # Convert from seconds to samples
    signal_length = len(wavsignal)
    frame_length = int(round(frame_length))
    frame_step = int(round(frame_step))
    num_frames = int(np.ceil(
        float(np.abs(signal_length - frame_length)) / frame_step))  # Make sure that we have at least 1 frame

    pad_signal_length = num_frames * frame_step + frame_length
    z = np.zeros((pad_signal_length - signal_length))
    pad_signal = np.append(wavsignal, z)
    # Pad Signal to make sure that all frames have equal number of samples without
    # truncating any samples from the original signal

    indices = np.tile(np.arange(0, frame_length), (num_frames, 1)) + np.tile(
        np.arange(0, num_frames * frame_step, frame_step), (frame_length, 1)).T
    frames = pad_signal[indices.astype(np.int32, copy=False)]
    # 3、加窗
    frames *= np.hanning(frame_length)
    # 4、傅立叶变换和功率谱
    NFFT = 512
    mag_frames = np.absolute(np.fft.rfft(frames, NFFT))  # Magnitude of the FFT
    pow_frames = ((1.0 / NFFT) * (mag_frames ** 2))  # Power Spectrum
    # 5、滤波器
    #    功率谱应用Mel刻度上的三角形滤波器（通常为40个滤波器）
    nfilt = 40
    low_freq_mel = 0
    high_freq_mel = (2595 * np.log10(1 + (fs / 2) / 700))  # Convert Hz to Mel
    mel_points = np.linspace(low_freq_mel, high_freq_mel, nfilt + 2)  # Equally spaced in Mel scale
    hz_points = (700 * (10 ** (mel_points / 2595) - 1))  # Convert Mel to Hz
    bin = np.floor((NFFT + 1) * hz_points / fs)
    fbank = np.zeros((nfilt, int(np.floor(NFFT / 2 + 1))))
    for m in range(1, nfilt + 1):
        f_m_minus = int(bin[m - 1])  # left
        f_m = int(bin[m])  # center
        f_m_plus = int(bin[m + 1])  # right

        for k in range(f_m_minus, f_m):
            fbank[m - 1, k] = (k - bin[m - 1]) / (bin[m] - bin[m - 1])
        for k in range(f_m, f_m_plus):
            fbank[m - 1, k] = (bin[m + 1] - k) / (bin[m + 1] - bin[m])
    filter_banks = np.dot(pow_frames, fbank.T)
    filter_banks = np.where(filter_banks == 0, np.finfo(float).eps, filter_banks)  # Numerical Stability
    filter_banks = 20 * np.log10(filter_banks)  # dB 得到频谱
    # 应用离散余弦变换（DCT）去相关滤波器组系数
    # num_ceps = 12
    # cep_lifter = 22
    # mfcc = dct(filter_banks, type=2, axis=1, norm='ortho')[:, 1: (num_ceps + 1)]  # Keep 2-13
    # # 将正弦提升器1应用于MFCC，以降低对较高MFCC的强调，这已被认为可以改善嘈杂信号中的语音识别。
    # (nframes, ncoeff) = mfcc.shape
    # n = np.arange(ncoeff)
    # lift = 1 + (cep_lifter / 2) * np.sin(np.pi * n / cep_lifter)
    # mfcc *= lift  # *
    # # 平均归一化
    # filter_banks -= (np.mean(filter_banks, axis=0) + 1e-8)
    # mfcc -= (np.mean(mfcc, axis=0) + 1e-8)
    return filter_banks


def source_get(source_file):
    # rootPath = "E:/DeepLearning/dl_studio/bishe/my_ch_speech_recognition-master/data/"
    rootPath = ""
    train_file = source_file
    pany_lst = []
    wav_lst = []
    han_lst = []
    for file in train_file:
        print('load', file, ' data ...')
        with open(file, 'r', encoding='utf8') as f:
            data = f.readlines()
        for line in tqdm(data):
            print(line)
            wav, pany, han = line.split('\t')
            wav_lst.append(rootPath + wav)
            pany_lst.append(pany.strip())
            han_lst.append(han.strip('\n'))
    return wav_lst, pany_lst, han_lst
    # for root, dirs, files in os.walk(train_file):
    #     for file in files:
    #         if file.endswith('.wav') or file.endswith('.WAV'):
    #             wav_file = os.sep.join([root, file])
    #             label_file = wav_file + '.trn'
    #             wav_lst.append(wav_file)
    #             label_lst.append(label_file)
    # return label_lst, wav_lst, han_lst


def word2id(line, vocab):
    return [vocab.index(pny) for pny in line.split(' ')]


# %%
def conv2d(size):
    return Conv2D(size, (3, 3), use_bias=True, activation='relu',
                  padding='same', kernel_initializer='he_normal')


def norm(x):
    return BatchNormalization(axis=-1)(x)


def maxpool(x):
    return MaxPooling2D(pool_size=(2, 2), strides=None, padding="valid")(x)


def dense(units, activation="relu"):
    return Dense(units, activation=activation, use_bias=True,
                 kernel_initializer='he_normal')


def cnn_cell(size, x, pool=True):
    x = norm(conv2d(size)(x))
    x = norm(conv2d(size)(x))
    if pool:
        x = maxpool(x)
    return x


def ctc_lambda(args):
    labels, y_pred, input_length, label_length = args
    y_pred = y_pred[:, :, :]
    return K.ctc_batch_cost(labels, y_pred, input_length, label_length)


def get_batch(batch_size, shuffle_list, wav_lst, label_data, vocab):
    for i in range(len(wav_lst) // batch_size):
        wav_data_lst = []
        label_data_lst = []
        begin = i * batch_size
        end = begin + batch_size
        sub_list = shuffle_list[begin:end]
        for index in sub_list:
            fbank = compute_fbank(wav_lst[index])
            fbank = fbank[:fbank.shape[0] // 8 * 8, :]
            label = word2id(label_data[index], vocab)
            wav_data_lst.append(fbank)
            label_data_lst.append(label)
        yield wav_data_lst, label_data_lst


def wav_padding(wav_data_lst):
    wav_lens = [len(data) for data in wav_data_lst]
    wav_max_len = max(wav_lens)
    wav_lens = np.array([leng // 8 for leng in wav_lens])
    nsize = 200
    new_wav_data_lst = np.zeros((len(wav_data_lst), wav_max_len, nsize, 1))
    for i in range(len(wav_data_lst)):
        new_wav_data_lst[i, :wav_data_lst[i].shape[0], :wav_data_lst[i].shape[1], 0] = wav_data_lst[i]
    return new_wav_data_lst, wav_lens


def label_padding(label_data_lst):
    label_lens = np.array([len(label) for label in label_data_lst])
    max_label_len = max(label_lens)
    new_label_data_lst = np.zeros((len(label_data_lst), max_label_len))
    for i in range(len(label_data_lst)):
        new_label_data_lst[i][:len(label_data_lst[i])] = label_data_lst[i]
    return new_label_data_lst, label_lens


def data_generator(batch_size, shuffle_list, wav_lst, label_data, vocab):
    for i in range(len(wav_lst) // batch_size):
        wav_data_lst = []
        label_data_lst = []
        begin = i * batch_size
        end = begin + batch_size
        sub_list = shuffle_list[begin:end]
        for index in sub_list:
            fbank = compute_fbank(wav_lst[index])
            pad_fbank = np.zeros((fbank.shape[0] // 8 * 8 + 8, fbank.shape[1]))
            pad_fbank[:fbank.shape[0], :] = fbank

            # print("1\n")
            # print(vocab)
            # print("2\n")
            # print(label_data[index])
            label = word2id(label_data[index], vocab)
            # print("3\n")
            # print(label)
            wav_data_lst.append(pad_fbank)

            label_data_lst.append(label)
        # print(wav_data_lst)
        # print(label_data_lst)
        pad_wav_data, input_length = wav_padding(wav_data_lst)

        pad_label_data, label_length = label_padding(label_data_lst)
        inputs = {'the_inputs': pad_wav_data,
                  'the_labels': pad_label_data,
                  'input_length': input_length,
                  'label_length': label_length,
                  }
        outputs = {'ctc': np.zeros(pad_wav_data.shape[0], )}
        yield inputs, outputs


def read_label(label_file):
    with open(label_file, 'r', encoding='utf8') as f:
        data = f.readlines()
        return data[1]


def gen_label_data(label_lst):
    label_data = []
    for label_file in label_lst:
        pny = read_label(label_file)
        label_data.append(pny.strip('\n'))
    return label_data


def decode_ctc(num_result, num2word):
    result = num_result[:, :, :]
    in_len = np.zeros((1), dtype=np.int32)
    in_len[0] = result.shape[1]
    r = K.ctc_decode(result, in_len, greedy=True, beam_width=10, top_paths=1)
    r1 = K.get_value(r[0][0])
    r1 = r1[0]
    text = []
    for i in r1:
        text.append(num2word[i])
    return r1, text


def mk_vocab(label_data):
    vocab = []
    for line in label_data:
        line = line.split(' ')
        for pny in line:
            if pny not in vocab:
                vocab.append(pny)
    vocab.append('_')
    return vocab


def mk_lm_pny_vocab(data):
    vocab = ['<PAD>']
    for line in data:
        line = line.split(' ')
        for pny in line:
            if pny not in vocab:
                vocab.append(pny)
    return vocab


def mk_lm_han_vocab(data):
    vocab = ['<PAD>']
    for line in tqdm(data):
        line = ''.join(line.split(' '))
        for han in line:
            if han not in vocab:
                vocab.append(han)
    return vocab

import difflib
def GetEditDistance(str1, str2):
    leven_cost = 0
    s = difflib.SequenceMatcher(None, str1, str2)
    for tag, i1, i2, j1, j2 in s.get_opcodes():
        if tag == 'replace':
            leven_cost += max(i2 - i1, j2 - j1)
        elif tag == 'insert':
            leven_cost += (j2 - j1)
        elif tag == 'delete':
            leven_cost += (i2 - i1)
    return leven_cost