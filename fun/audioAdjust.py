# -*- coding: UTF-8 -*-
import librosa
import cv2
from scipy.io import wavfile
import os
import numpy as np


def data_augmentation(input_audio, output_audio):  # 时间拉伸
    in_path = os.path.dirname(input_audio)
    out_path = os.path.dirname(output_audio)
    if not os.path.isdir(out_path):  # 判断目标路径是否存在，不存在则创建
        os.mkdir(out_path)
    y, sr = librosa.load(input_audio, sr=None)
    for rate in [0.81, 0.93, 1.07, 1.23]:  # 拉伸速率
        y_shift = librosa.effects.time_stretch(y, rate=rate)  # 使用TS生成新数据
        # 数据导出为文件  replace('.ogg', '_no-{}.wav'.format(step)))  (old,new)
        librosa.output.write_wav(os.path.join(out_path,
                                              os.path.basename(input_audio).replace('.ogg', '_ts-{}.wav'.format(rate))),
                                 y_shift, sr)
    for step1 in [-2, -1, 1, 2]:
        y_shift1 = librosa.effects.pitch_shift(y, sr, n_steps=step1)  # 使用PS1生成新数据
        librosa.output.write_wav(os.path.join(out_path,
                                              os.path.basename(input_audio).replace('.ogg',
                                                                                    '_ps1-{}.wav'.format(step1))),
                                 y_shift, sr)
    for step2 in [-3.5, -2.5, 2.5, 3.5]:
        y_shift = librosa.effects.pitch_shift(y, sr, n_steps=step2)  # 使用PS2生成新数据
        librosa.output.write_wav(os.path.join(out_path,
                                              os.path.basename(input_audio).replace('.ogg',
                                                                                    '_ps2-{}.wav'.format(step2))),
                                 y_shift, sr)
    for step3 in [1, 2, 3, 4]:
        wn = np.random.normal(0, 1, len(y))  # 从高斯分布（正态分布）提取样本
        y_noise = np.where(y != 0.0, y.astype('float64') + 0.02 * wn, 0.0).astype(np.float32)
        librosa.output.write_wav(os.path.join(out_path,
                                              os.path.basename(input_audio).replace('.ogg',
                                                                                    "_no-{}.wav".format(step3))),
                                 y_noise, sr)


index = 0
file_path = "E:/DeepLearning/dl_studio/bishe/Audio/MyAudioData/"  # 输入文件夹路径
save_path = "E:/DeepLearning/dl_studio/bishe/Audio/MyAudioData_max/"  # 增强后数据(输出)文件夹路径[无文件夹会自动创建]

for filename in os.listdir(file_path):
    print(filename)
    in_path = file_path + filename
    out_path = save_path + filename
    data_augmentation(in_path, out_path)  # 数据增强 4*4
    index = index + 1
    print(index)
    print()
    cv2.waitKey(200)
