# -*- coding: UTF-8 -*-
import pyaudio
import wave
import os
import time
import random

input_fileName = "personInput.wav"
input_filePath = "./MyAudioData/"
in_path = input_filePath + input_fileName


def get_Audio(filePath):
    strA = "是"
    if strA == "是":
        Chunk = 1024
        Format = pyaudio.paInt16
        Channels = 1  # 声道数
        Rate = 16000  # 采样率
        Record_seconds = 5
        Wave_Output_FileName = filePath
        p = pyaudio.PyAudio()
        stream = p.open(format=Format,
                        channels=Channels,
                        rate=Rate,
                        input=True,
                        frames_per_buffer=Chunk)

        print("*" * 10, "开始录音：请在5s内输入语音")
        frames = []
        for i in range(0, int(Rate / Chunk * Record_seconds)):
            data = stream.read(Chunk)
            frames.append(data)

        print("*" * 10, "录音结束\n")

        stream.stop_stream()
        stream.close()
        p.terminate()

        wf = wave.open(Wave_Output_FileName, 'wb')
        wf.setnchannels(Channels)
        wf.setsampwidth(p.get_sample_size(Format))
        wf.setframerate(Rate)
        wf.writeframes(b''.join(frames))
        wf.close()
    elif strA == "否":
        exit()
    else:
        print("输入错误")
        get_Audio(in_path)


if __name__ == '__main__':
    root = "./data/"
    if not os.path.exists(root):
        os.mkdir(root)
    dist = ['上一首', '下一首', '暂停播放', '打开音响', '关闭音响', '播放音乐']
    dist2 = [1, 2, 4, 5, 6, 3]
    dist3 = ['shangyishou', 'xiayishou', 'zantinbofang', 'dakaiyinxiang', 'guanbiyinxiang', 'bofangyinyue']
    for j in range(6):
        if not os.path.exists(root + "/" + str(dist2[j])):
            os.mkdir(root + "/" + str(dist2[j]))
        # root += str(dist2[j])
        print("开始录取==>" + dist[j])
        for i in range(3):
            print("请说普通话：" + dist[j])
            random_ = int(random.random()) % 10000 + int(200 * random.random() % 1000)
            path = root + '/' + str(dist2[j]) + '/' + dist3[j] + "_" + str(random_) + ".wav"
            if not os.path.isfile(path):  # 无文件时创建
                fd = open(path, mode="w", encoding="utf-8")
                fd.close()
            get_Audio(path)
            print("休息2s")
            time.sleep(2)
        print("休息10s，开始下一个类别")
        time.sleep(10)
