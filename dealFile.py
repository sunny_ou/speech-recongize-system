import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os
from xpinyin import Pinyin

root = "D:\\pycharmProject\\data\\haiwav"

# 读取所有的文件夹
listdirs = os.listdir(root)
p = Pinyin()
with open('hai_train.txt', 'w', encoding='UTF-8') as f:
    for dir in listdirs:
        path = root+"\\"+dir
        # print(path)
        files = os.listdir(path)
        # print(files)
        for file in files:
            filepath = path+"\\"+file
            # print(filepath)
            hanzi = dir
            ret = p.get_pinyin(hanzi, tone_marks="numbers").replace('-', ' ')
            pinyin = ret
            dataLine = filepath + "\t" + pinyin + "\t" + hanzi + "\n"
            print(dataLine)
            f.write(dataLine)
f.close()
# print(listdirs)
