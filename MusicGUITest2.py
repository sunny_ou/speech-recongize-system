import requests
import time
import json
import os

musicDist = {}
os.system('')
keyword = input('\033[33m来源：酷狗音乐(少数付费歌曲只能下载试听的一分钟）\n搜索歌曲:\033[0m')
time = time.time() * 1000
url = f'https://songsearch.kugou.com/song_search_v2?callback=jQuery112405132987859127838_{time}' \
      f'&page=1&pagesize=30&userid=-1&clientver=&platform=WebFilter&tag=em&filter=2&iscorrection=1&privilege_filter=0&_={time}&keyword={keyword}'
headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36'}
resp1 = requests.get(url, headers=headers).text
s1 = resp1.find('{')
resp1 = resp1[s1:-2]
jd = json.loads(resp1)  # 字符串转字典
for i in jd['data']['lists']:
    a = i['FileHash']
    b = i['AlbumID']
    url1 = f'https://wwwapi.kugou.com/yy/index.php?r=play/getdata&callback' \
           f'=jQuery19108991040761831859_1607742705511&hash=' \
           f'{a}&dfid=334lgQ3gvzHD1503TJ1eRVym&mid=449fc6a9349f6b64fc0d58efab406b8d&platid=4&album_id' \
           f'={b}&_=1607742705512'
    resp2 = requests.get(url=url1, headers=headers).text
    s2 = resp2.find('{')
    resp2 = resp2[s2:-2]
    jd1 = json.loads(resp2)  # 字符串转字典
    print('歌名\033[36m', jd1['data']['audio_name'])
    print('\033[0m专辑名', jd1['data']['album_name'])
    print('下载链接\033[33m', jd1['data']['play_url'], '\033[0m')
    musicDist[jd1['data']['audio_name']] = jd1['data']['play_url']
input('\033[32m按enter退出\033[0m')
