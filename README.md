<<<<<<< HEAD
## 1、首先安装依赖：使用文件SpeechRecongizeGUI.yaml进行安装
## 2、然后运行GUI_Speech.py即可打开GUI客户端
点击Start，然后选择一个wav文件路径即可开始录音

然后开始说话，说话完毕后点击Stop

最后得到结果：



## 注：该模型当前只加入了孤立词：播放音乐、打开音响、关闭音响、上一首、下一首、暂停播放
## 如果要自己进行训练模型需要单独制作数据集，并使用train.py进行训练，录音数据需要单独导入
=======
## 1、首先安装依赖：使用文件SpeechRecongizeGUI.yaml进行安装
## 2、然后运行GUI_Speech.py即可打开GUI客户端
点击Start，然后选择一个wav文件路径即可开始录音

![输入图片说明](https://images.gitee.com/uploads/images/2021/0115/142441_8bbc199d_7397701.png "屏幕截图.png")

然后开始说话，说话完毕后点击Stop

最后得到结果：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0115/142557_9ff52780_7397701.png "屏幕截图.png")

## 注：该模型当前只加入了孤立词：播放音乐、打开音响、关闭音响、上一首、下一首、暂停播放
## 如果要自己进行训练模型需要单独制作数据集，并使用train.py进行训练t，录音.........!!!!!!



## 使用自己数据集训练：
1、需要把每个类别的数据集导入到同一个文件夹：
例如：![输入图片说明](https://images.gitee.com/uploads/images/2021/0129/163210_f9ba24e0_7397701.png "屏幕截图.png")

2、通过dealFile.py把标签和wav文件路径写入到txt文件中：
注意：修改为自己电脑的目录：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0129/163339_a3606e2c_7397701.png "屏幕截图.png")

3、得到txt文件：格式为：wav文件路径+拼音+中文
![输入图片说明](https://images.gitee.com/uploads/images/2021/0129/163411_4a7e9e42_7397701.png "屏幕截图.png")

4、运行train.py进行训练

5、测试：使用SpeechRecongize.py进行测试
![输入图片说明](https://images.gitee.com/uploads/images/2021/0129/163516_c3f95f42_7397701.png "屏幕截图.png")
