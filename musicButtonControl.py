import tkinter
import pygame
import os
import pyaudio, wave, threading
from TestRe.SpeechRecongize import getText


class MusicButtonControl(tkinter.Frame):
    def __init__(self, master, otherMusicList):
        self.fileName = None
        self.allowRecording = True
        self.CHUNK_SIZE = 1024
        self.CHANNELS = 1
        self.FORMAT = pyaudio.paInt16
        self.RATE = 16000

        self.askquestion = "no"
        self.frame = tkinter.Frame(master)
        self.frame.pack(side=tkinter.TOP, fill=tkinter.Y)

        # 加载音乐列表
        self.otherMusicList = otherMusicList
        self.loadMusic()
        print("======音乐加载完成")

        self.buttonPlay = tkinter.Button(self.frame, text="播放",
                                         command=self.playMusic,
                                         width=8, height=2, bg='#FFEC8B')
        self.buttonPlay.pack(side=tkinter.LEFT, fill=tkinter.X)

        self.buttonPause = tkinter.Button(self.frame, text="暂停",
                                          command=self.pauseMusic,
                                          width=8, height=2, bg='#FFEC8B')
        self.buttonPause.pack(side=tkinter.LEFT, fill=tkinter.X)

        self.buttonStop = tkinter.Button(self.frame, text="停止",
                                         command=self.stopMusic,
                                         width=8, height=2, bg='#FFEC8B')
        self.buttonStop.pack(side=tkinter.LEFT, fill=tkinter.X)

        self.buttonPrevious = tkinter.Button(self.frame, text="上一曲",
                                             command=self.previousMusic,
                                             width=8, height=2, bg='#FFEC8B')
        self.buttonPrevious.pack(side=tkinter.LEFT, fill=tkinter.X)

        self.buttonNext = tkinter.Button(self.frame, text="下一曲",
                                         command=self.nextMusic,
                                         width=8, height=2, bg='#FFEC8B')
        self.buttonNext.pack(side=tkinter.LEFT, fill=tkinter.X)

        # 语音控制
        self.binStart = tkinter.Button(self.frame, text='Start', command=self.start,
                                       width=8, height=2, bg="#DC143C")
        self.binStart.pack(side=tkinter.LEFT, fill=tkinter.X)

        self.binStop = tkinter.Button(self.frame, text='Stop', command=self.stop,
                                      width=8, height=2, bg="#DC143C")
        self.binStop.pack(side=tkinter.LEFT, fill=tkinter.X)

        self.retext = tkinter.StringVar(value="略")
        self.labelInfo = tkinter.Label(master, textvariable=self.retext, height=3, width=20)
        self.labelInfo.pack(fill=tkinter.Y, side=tkinter.TOP)

        # self.textLyric = tkinter.Text(self.frame, bg="#FFDEAD", height=50)
        # self.textLyric.pack(side=tkinter.LEFT, fill=tkinter.X)

    def loadMusic(self):
        pygame.mixer.init()
        # self.otherMusicList.listBox.select_set(0)
        # musicFilePath = r"E:\Python-1704\python\day15\homework\播放音乐器\music"
        # print(self.otherMusicList.listBox.size())
        # print("播放：", self.otherMusicList.getMusicPath())

    def playMusic(self):
        pygame.mixer.music.load(self.otherMusicList.getCurrentMusicPath())
        pygame.mixer.music.play()
        print(self.otherMusicList.getCurrentMusicPath())

    def pauseMusic(self):
        pygame.mixer.music.pause()

    def stopMusic(self):
        pygame.mixer.music.stop()

    def previousMusic(self):
        path = r"D:\pycharmProject\TestRe\musicPath"
        currentMusicPath = self.otherMusicList.getCurrentMusicPath()

        for musicpathIndex in range(self.otherMusicList.listBox.size()):
            ismusic = 0
            musicAbs1Path = path + "\\" + self.otherMusicList.listBox.get(musicpathIndex)
            if currentMusicPath == musicAbs1Path:
                ismusic = musicpathIndex
                ismusic -= 1
                musicAbsPath = path + "\\" + self.otherMusicList.listBox.get(ismusic)
                if ismusic < 0:
                    pygame.mixer.music.load(path + "\\" + self.otherMusicList.listBox.get(0))
                    pygame.mixer.music.play()
                    break
                pygame.mixer.music.load(musicAbsPath)
                # 显示正在播放的歌曲，并取消上一首歌曲的选中
                self.otherMusicList.listBox.select_clear(musicpathIndex)
                self.otherMusicList.listBox.select_set(ismusic)
                pygame.mixer.music.play()
                break

    def nextMusic(self):
        path = r"D:\pycharmProject\TestRe\musicPath"
        currentMusicPath = self.otherMusicList.getCurrentMusicPath()

        for musicpathIndex in range(self.otherMusicList.listBox.size()):
            ismusic = 0
            musicAbs1Path = path + "\\" + self.otherMusicList.listBox.get(musicpathIndex)
            if currentMusicPath == musicAbs1Path:
                ismusic = musicpathIndex
                ismusic += 1
                if ismusic >= self.otherMusicList.listBox.size():
                    pygame.mixer.music.load(
                        path + "\\" + self.otherMusicList.listBox.get(self.otherMusicList.listBox.size() - 1))
                    pygame.mixer.music.play()
                    break
                musicAbsPath = path + "\\" + self.otherMusicList.listBox.get(ismusic)
                pygame.mixer.music.load(musicAbsPath)
                # 显示正在播放的歌曲，并取消上一首歌曲的选中
                self.otherMusicList.listBox.select_clear(musicpathIndex)
                self.otherMusicList.listBox.select_set(ismusic)
                pygame.mixer.music.play()
                break

    def record(self):
        print("开始录音")
        p = pyaudio.PyAudio()
        # audio流对象
        stream = p.open(format=self.FORMAT,
                        channels=self.CHANNELS,
                        rate=self.RATE,
                        input=True,
                        frames_per_buffer=self.CHUNK_SIZE)
        # 音频文件对象
        print("写入音频的fileName:", self.fileName)
        # 读取数据写入文件
        frames = []
        while self.allowRecording:
            # print("录音中...")
            data = stream.read(self.CHUNK_SIZE)
            frames.append(data)
            # if self.askquestion == "yes":
            #     print("<<<结束录音>>")
            #     break

        stream.stop_stream()
        stream.close()
        p.terminate()
        # 保存到wav文件
        wf = wave.open(self.fileName, 'wb')
        wf.setnchannels(self.CHANNELS)
        wf.setsampwidth(p.get_sample_size(self.FORMAT))
        wf.setframerate(self.RATE)
        wf.writeframes(b''.join(frames))
        wf.close()
        print("结束录音")
        # fileName = None

    def predict(self):
        text = "未识别"
        # 载入wav文件 和 模型
        print("识别文件名:", self.fileName)
        hanzitext = getText(self.fileName)
        print("识别结果：", hanzitext)
        text = hanzitext
        # showinfo = tkinter.messagebox.showinfo(title="识别结果", message="识别结果：" + text)
        self.retext.set("识别结果：" + text)
        # lbSpeech_Recognition['text'] = "识别结果：" + text
        # if text in "上一首":
        #     self.previousMusic()
        # elif text in "下一首":
        #     self.nextMusic()
        # elif text in "暂停播放":
        #     self.pauseMusic()
        # elif text in "停止播放":
        #     self.stopMusic()
        # elif text in "播放" or text in "继续播放":
        #     self.playMusic()

    def start(self):
        self.fileName = tkinter.filedialog.asksaveasfilename(filetypes=[('未压缩文件', '*.wav')])
        # self.fileName = './TestWav/test.wav'
        if not self.fileName:
            return
        if not self.fileName.endswith('.wav'):
            self.fileName = self.fileName + '.wav'
        self.allowRecording = True
        # lbStatus['text'] = '状态：录音Recoding...'
        # 显示录取框
        # self.askquestion = tkinter.messagebox.askquestion(title="录音中...", message="正在录音,是否结束?")
        # 监听是否录取完成
        threading.Thread(target=self.record).start()

    def stop(self):
        import time
        # while self.askquestion != "yes":
        #     # print("等待识别")
        #     pass
        # 休眠1s防止文件未保存
        # print("开始休眠等待识别")
        # time.sleep(1)
        self.allowRecording = False
        # lbStatus['text'] = '状态：准备录音Ready'
        # lbSpeech_Recognition['text'] = "正在识别,请稍等...."
        # 开始识别
        print("======开始识别======")
        time.sleep(3)
        self.predict()
