import tkinter
import os
import time


class MusicLyricText(tkinter.Frame):
    def __init__(self, master):
        self.frame = tkinter.Frame(master)
        self.frame.pack(side=tkinter.TOP, fill=tkinter.Y)

        self.textLyric = tkinter.Text(self.frame, bg="#FFDEAD", height=50)
        self.textLyric.pack()
